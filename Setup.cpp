/*
 * Setup.cpp
 *
 *  Created on: Feb 16, 2019
 *      Author: ryan
 */
#include "Setup.hpp"

CRGB leds_plus_safety_pixel[NUM_LEDS + 1];
extern CRGB* const leds(leds_plus_safety_pixel + 1);

uint16_t XY(uint8_t x, uint8_t y)
{
    uint16_t i = (y * kMatrixWidth) + x;
    return i;
}

uint16_t XYsafe(uint8_t x, uint8_t y)
{
  if( x >= kMatrixWidth) return -1;
  if( y >= kMatrixHeight) return -1;
  return XY(x,y);
}

void initLedMatrix(int SerialBaud)
{
    // 1 second delay for recovery
    delay(1000);

    Serial.begin(SerialBaud);

    // Add Strip Configuration
    FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

    // Master Brightness
    FastLED.setBrightness(BRIGHTNESS);

    pinMode(LED_BUILTIN,OUTPUT);

    toggleLED(LED_BUILTIN,100,10);

    Serial.println(F("<Arduino Initialized>"));

#ifdef DEBUG
     Serial.println(F("DEBUG MODE"));
     Serial.println(F("\n"));
#else
     Serial.println(F("RELEASE MODE"));
     Serial.println(F("\n"));
#endif

}

void toggleLED(uint8_t port, timeMilli delayAfterToggle, uint8_t iterations)
{
	if (port == NOT_A_PIN) return;
	for (uint8_t iterations_ = 0; iterations_ < iterations; ++iterations_)
	{
		if (digitalRead(port) == HIGH)
		{
			digitalWrite(port,LOW);
		} else
		{
			digitalWrite(port,HIGH);
		}
		delay(delayAfterToggle);
	}
}
