#include "Matrix.hpp"

void setup()
{
	initLedMatrix(9600);
}

// Import Assets
#include "assets/Animation_Cane.hpp"
#include "assets/Animation_Heart.hpp"
#include "assets/Animation_Snowflake.hpp"
#include "assets/Animation_Tree.hpp"
#include "assets/Animation_Snow.hpp"
#include "assets/Animation_Shamrock.hpp"
#include "assets/Animation_Egga.hpp"
#include "assets/Animation_Eggb.hpp"

void loop()
{
	drawAnimation(ANIMATION_HEART, 50, 1000);

	drawAnimation(ANIMATION_TREE, 50, 50);
	drawAnimation(ANIMATION_TREE, 50, 1000, true);

	drawAnimation(ANIMATION_SNOW, 50, 1000);

	drawAnimation(ANIMATION_CANE, 50, 1000);

	drawAnimation(ANIMATION_SNOWFLAKE, 50, 1000);

	drawAnimation(ANIMATION_SHAMROCK, 50, 1000);

	drawAnimation(ANIMATION_EGGA, 50, 1000);
	drawAnimation(ANIMATION_EGGB, 50, 1000, true);

	drawAnimation(ANIMATION_EGGB, 50, 1000);
	drawAnimation(ANIMATION_EGGA, 50, 1000, true);

	toggleLED(LED_BUILTIN);
}
