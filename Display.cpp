/*
 * Display.cpp
 *
 *  Created on: Feb 16, 2019
 *      Author: ryan
 */

#include "Display.hpp"

void drawPicture(const Picture picture, timeMilli delayAfterPicture)
{
	Pxlptr pptr = picture;

	// Store the background color specified in the line with PICTURE_NULL
	while(pptr->posX != PICTURE_NULL) {++pptr;}
	uint8_t backgroundR = pptr->red;
	uint8_t backgroundG = pptr->green;
	uint8_t backgroundB = pptr->blue;
	pptr = picture;

	// Iterate through entire grid, setting color if pixel is found otherwise set to background
	for (uint8_t curY = 0; curY <= kMatrixHeight - 1 /*9*/; ++curY)
	{
		for (uint8_t curX = 0; curX <= kMatrixWidth - 1 /*9*/; ++curX)
		{
			if ((pptr->posX == curX) && (pptr->posY == curY))
			{
				leds[XYsafe((pptr->posX), pptr->posY)] = CRGB(pptr->red, pptr->green, pptr->blue);
				++pptr;
			} else
			{
				leds[XYsafe(curX,curY)].setRGB(backgroundR, backgroundG, backgroundB);
			}
		}
	}

	FastLED.show();
	FastLED.delay(delayAfterPicture);
}

void drawAnimation(const Animation animation, timeMilli frameInterval, timeMilli delayAfterAnimation, bool reverse, uint16_t iterations)
{
	// Variables
	Pxlptr aptr = animation;
	// Saves each frame into array after getting the number of frames
	while (aptr->posX != ANIMATION_NULL) {++aptr;}
	uint8_t numFrames = aptr->posY;
	Pxlptr frameIndex[numFrames];
	aptr = animation;	// Reset pointer to save frames

	uint8_t curFramePos = 0;
	while (aptr->posX != ANIMATION_NULL)
	{
		++aptr;
		if (aptr->posX == PICTURE_NULL)
		{
			frameIndex[curFramePos] = aptr - aptr->posY;
			++curFramePos;
		}
	}

	// Draw animation "iterations" times
	for (uint8_t iterations_ = 0; iterations_ < iterations; ++iterations_)
	{
		// Iterate across array backwards if reverse if true, otherwise iterate across array in forward direction
		for (uint8_t i = ((reverse) ? (numFrames - 1) : (0));
			(reverse) ? (i != 255) : (i <= numFrames - 1); 		// Value i underflows from 0 to 255
			((reverse) ? (--i) : (++i)))
		{
			#ifdef DEBUG
			Serial.print(F("Index "));
			Serial.print(i);
			Serial.print(F(" : "));
			Serial.print(frameIndex[i]->posX);
			Serial.print(F("  "));
			Serial.print(frameIndex[i]->posY);
			Serial.print(F("  "));
			Serial.print(frameIndex[i]->red);
			Serial.print(F("  "));
			Serial.print(frameIndex[i]->green);
			Serial.print(F("  "));
			Serial.println(frameIndex[i]->blue);
			#endif

			drawPicture(frameIndex[i]);
			FastLED.delay(frameInterval);
		}
	}

	#ifdef DEBUG
	Serial.println(F("Finished animation"));
	Serial.println();
	#endif

	FastLED.delay(delayAfterAnimation);
}

