#ifndef ANIMATION_EGGA_HPP
#define ANIMATION_EGGA_HPP

#include "../Display.hpp"

PROGMEM const Animation ANIMATION_EGGA = {
    {3,1,0,166,255},
    {4,1,0,166,255},
    {5,1,0,166,255},
    {6,1,0,166,255},
    {2,2,0,166,255},
    {3,2,255,255,255},
    {4,2,255,255,255},
    {5,2,255,255,255},
    {6,2,255,255,255},
    {7,2,0,166,255},
    {1,3,0,166,255},
    {2,3,255,255,255},
    {3,3,255,255,255},
    {4,3,255,255,255},
    {5,3,255,255,255},
    {6,3,255,255,255},
    {7,3,255,255,255},
    {8,3,0,166,255},
    {1,4,0,166,255},
    {2,4,255,0,38},
    {3,4,255,0,38},
    {4,4,255,0,38},
    {5,4,255,0,38},
    {6,4,255,0,38},
    {7,4,255,0,38},
    {8,4,0,166,255},
    {0,5,0,166,255},
    {1,5,255,255,255},
    {2,5,255,255,255},
    {3,5,255,255,255},
    {4,5,255,255,255},
    {5,5,255,255,255},
    {6,5,255,255,255},
    {7,5,255,255,255},
    {8,5,255,255,255},
    {9,5,0,166,255},
    {0,6,0,166,255},
    {1,6,255,255,255},
    {2,6,255,255,255},
    {3,6,255,255,255},
    {4,6,255,255,255},
    {5,6,255,255,255},
    {6,6,255,255,255},
    {7,6,255,255,255},
    {8,6,255,255,255},
    {9,6,0,166,255},
    {0,7,0,166,255},
    {1,7,255,255,0},
    {2,7,255,255,0},
    {3,7,255,255,0},
    {4,7,255,255,0},
    {5,7,255,255,0},
    {6,7,255,255,0},
    {7,7,255,255,0},
    {8,7,255,255,0},
    {9,7,0,166,255},
    {0,8,0,166,255},
    {1,8,255,255,255},
    {2,8,255,255,255},
    {3,8,255,255,255},
    {4,8,255,255,255},
    {5,8,255,255,255},
    {6,8,255,255,255},
    {7,8,255,255,255},
    {8,8,255,255,255},
    {9,8,0,166,255},
    {0,9,0,166,255},
    {1,9,255,255,255},
    {2,9,255,255,255},
    {3,9,255,255,255},
    {4,9,255,255,255},
    {5,9,255,255,255},
    {6,9,255,255,255},
    {7,9,255,255,255},
    {8,9,255,255,255},
    {9,9,0,166,255},
    {PICTURE_NULL,76,10,10,10},

    {3,0,0,166,255},
    {4,0,0,166,255},
    {5,0,0,166,255},
    {6,0,0,166,255},
    {2,1,0,166,255},
    {3,1,255,255,255},
    {4,1,255,255,255},
    {5,1,255,255,255},
    {6,1,255,255,255},
    {7,1,0,166,255},
    {1,2,0,166,255},
    {2,2,255,255,255},
    {3,2,255,255,255},
    {4,2,255,255,255},
    {5,2,255,255,255},
    {6,2,255,255,255},
    {7,2,255,255,255},
    {8,2,0,166,255},
    {1,3,0,166,255},
    {2,3,255,0,38},
    {3,3,255,0,38},
    {4,3,255,0,38},
    {5,3,255,0,38},
    {6,3,255,0,38},
    {7,3,255,0,38},
    {8,3,0,166,255},
    {0,4,0,166,255},
    {1,4,255,255,255},
    {2,4,255,255,255},
    {3,4,255,255,255},
    {4,4,255,255,255},
    {5,4,255,255,255},
    {6,4,255,255,255},
    {7,4,255,255,255},
    {8,4,255,255,255},
    {9,4,0,166,255},
    {0,5,0,166,255},
    {1,5,255,255,255},
    {2,5,255,255,255},
    {3,5,255,255,255},
    {4,5,255,255,255},
    {5,5,255,255,255},
    {6,5,255,255,255},
    {7,5,255,255,255},
    {8,5,255,255,255},
    {9,5,0,166,255},
    {0,6,0,166,255},
    {1,6,255,255,0},
    {2,6,255,255,0},
    {3,6,255,255,0},
    {4,6,255,255,0},
    {5,6,255,255,0},
    {6,6,255,255,0},
    {7,6,255,255,0},
    {8,6,255,255,0},
    {9,6,0,166,255},
    {0,7,0,166,255},
    {1,7,255,255,255},
    {2,7,255,255,255},
    {3,7,255,255,255},
    {4,7,255,255,255},
    {5,7,255,255,255},
    {6,7,255,255,255},
    {7,7,255,255,255},
    {8,7,255,255,255},
    {9,7,0,166,255},
    {0,8,0,166,255},
    {1,8,255,255,255},
    {2,8,255,255,255},
    {3,8,255,255,255},
    {4,8,255,255,255},
    {5,8,255,255,255},
    {6,8,255,255,255},
    {7,8,255,255,255},
    {8,8,255,255,255},
    {9,8,0,166,255},
    {1,9,0,166,255},
    {2,9,0,255,35},
    {3,9,0,255,35},
    {4,9,0,255,35},
    {5,9,0,255,35},
    {6,9,0,255,35},
    {7,9,0,255,35},
    {8,9,0,166,255},
    {PICTURE_NULL,84,10,10,10},

    {2,0,0,166,255},
    {3,0,255,255,255},
    {4,0,255,255,255},
    {5,0,255,255,255},
    {6,0,255,255,255},
    {7,0,0,166,255},
    {1,1,0,166,255},
    {2,1,255,255,255},
    {3,1,255,255,255},
    {4,1,255,255,255},
    {5,1,255,255,255},
    {6,1,255,255,255},
    {7,1,255,255,255},
    {8,1,0,166,255},
    {1,2,0,166,255},
    {2,2,255,0,38},
    {3,2,255,0,38},
    {4,2,255,0,38},
    {5,2,255,0,38},
    {6,2,255,0,38},
    {7,2,255,0,38},
    {8,2,0,166,255},
    {0,3,0,166,255},
    {1,3,255,255,255},
    {2,3,255,255,255},
    {3,3,255,255,255},
    {4,3,255,255,255},
    {5,3,255,255,255},
    {6,3,255,255,255},
    {7,3,255,255,255},
    {8,3,255,255,255},
    {9,3,0,166,255},
    {0,4,0,166,255},
    {1,4,255,255,255},
    {2,4,255,255,255},
    {3,4,255,255,255},
    {4,4,255,255,255},
    {5,4,255,255,255},
    {6,4,255,255,255},
    {7,4,255,255,255},
    {8,4,255,255,255},
    {9,4,0,166,255},
    {0,5,0,166,255},
    {1,5,255,255,0},
    {2,5,255,255,0},
    {3,5,255,255,0},
    {4,5,255,255,0},
    {5,5,255,255,0},
    {6,5,255,255,0},
    {7,5,255,255,0},
    {8,5,255,255,0},
    {9,5,0,166,255},
    {0,6,0,166,255},
    {1,6,255,255,255},
    {2,6,255,255,255},
    {3,6,255,255,255},
    {4,6,255,255,255},
    {5,6,255,255,255},
    {6,6,255,255,255},
    {7,6,255,255,255},
    {8,6,255,255,255},
    {9,6,0,166,255},
    {0,7,0,166,255},
    {1,7,255,255,255},
    {2,7,255,255,255},
    {3,7,255,255,255},
    {4,7,255,255,255},
    {5,7,255,255,255},
    {6,7,255,255,255},
    {7,7,255,255,255},
    {8,7,255,255,255},
    {9,7,0,166,255},
    {1,8,0,166,255},
    {2,8,0,255,35},
    {3,8,0,255,35},
    {4,8,0,255,35},
    {5,8,0,255,35},
    {6,8,0,255,35},
    {7,8,0,255,35},
    {8,8,0,166,255},
    {1,9,0,166,255},
    {2,9,255,255,255},
    {3,9,255,255,255},
    {4,9,255,255,255},
    {5,9,255,255,255},
    {6,9,255,255,255},
    {7,9,255,255,255},
    {8,9,0,166,255},
    {PICTURE_NULL,88,10,10,10},

    {1,0,0,166,255},
    {2,0,255,255,255},
    {3,0,255,255,255},
    {4,0,255,255,255},
    {5,0,255,255,255},
    {6,0,255,255,255},
    {7,0,255,255,255},
    {8,0,0,166,255},
    {1,1,0,166,255},
    {2,1,255,0,38},
    {3,1,255,0,38},
    {4,1,255,0,38},
    {5,1,255,0,38},
    {6,1,255,0,38},
    {7,1,255,0,38},
    {8,1,0,166,255},
    {0,2,0,166,255},
    {1,2,255,255,255},
    {2,2,255,255,255},
    {3,2,255,255,255},
    {4,2,255,255,255},
    {5,2,255,255,255},
    {6,2,255,255,255},
    {7,2,255,255,255},
    {8,2,255,255,255},
    {9,2,0,166,255},
    {0,3,0,166,255},
    {1,3,255,255,255},
    {2,3,255,255,255},
    {3,3,255,255,255},
    {4,3,255,255,255},
    {5,3,255,255,255},
    {6,3,255,255,255},
    {7,3,255,255,255},
    {8,3,255,255,255},
    {9,3,0,166,255},
    {0,4,0,166,255},
    {1,4,255,255,0},
    {2,4,255,255,0},
    {3,4,255,255,0},
    {4,4,255,255,0},
    {5,4,255,255,0},
    {6,4,255,255,0},
    {7,4,255,255,0},
    {8,4,255,255,0},
    {9,4,0,166,255},
    {0,5,0,166,255},
    {1,5,255,255,255},
    {2,5,255,255,255},
    {3,5,255,255,255},
    {4,5,255,255,255},
    {5,5,255,255,255},
    {6,5,255,255,255},
    {7,5,255,255,255},
    {8,5,255,255,255},
    {9,5,0,166,255},
    {0,6,0,166,255},
    {1,6,255,255,255},
    {2,6,255,255,255},
    {3,6,255,255,255},
    {4,6,255,255,255},
    {5,6,255,255,255},
    {6,6,255,255,255},
    {7,6,255,255,255},
    {8,6,255,255,255},
    {9,6,0,166,255},
    {1,7,0,166,255},
    {2,7,0,255,35},
    {3,7,0,255,35},
    {4,7,0,255,35},
    {5,7,0,255,35},
    {6,7,0,255,35},
    {7,7,0,255,35},
    {8,7,0,166,255},
    {1,8,0,166,255},
    {2,8,255,255,255},
    {3,8,255,255,255},
    {4,8,255,255,255},
    {5,8,255,255,255},
    {6,8,255,255,255},
    {7,8,255,255,255},
    {8,8,0,166,255},
    {2,9,0,166,255},
    {3,9,255,255,255},
    {4,9,255,255,255},
    {5,9,255,255,255},
    {6,9,255,255,255},
    {7,9,0,166,255},
    {PICTURE_NULL,88,10,10,10},

    {1,0,0,166,255},
    {2,0,255,0,38},
    {3,0,255,0,38},
    {4,0,255,0,38},
    {5,0,255,0,38},
    {6,0,255,0,38},
    {7,0,255,0,38},
    {8,0,0,166,255},
    {0,1,0,166,255},
    {1,1,255,255,255},
    {2,1,255,255,255},
    {3,1,255,255,255},
    {4,1,255,255,255},
    {5,1,255,255,255},
    {6,1,255,255,255},
    {7,1,255,255,255},
    {8,1,255,255,255},
    {9,1,0,166,255},
    {0,2,0,166,255},
    {1,2,255,255,255},
    {2,2,255,255,255},
    {3,2,255,255,255},
    {4,2,255,255,255},
    {5,2,255,255,255},
    {6,2,255,255,255},
    {7,2,255,255,255},
    {8,2,255,255,255},
    {9,2,0,166,255},
    {0,3,0,166,255},
    {1,3,255,255,0},
    {2,3,255,255,0},
    {3,3,255,255,0},
    {4,3,255,255,0},
    {5,3,255,255,0},
    {6,3,255,255,0},
    {7,3,255,255,0},
    {8,3,255,255,0},
    {9,3,0,166,255},
    {0,4,0,166,255},
    {1,4,255,255,255},
    {2,4,255,255,255},
    {3,4,255,255,255},
    {4,4,255,255,255},
    {5,4,255,255,255},
    {6,4,255,255,255},
    {7,4,255,255,255},
    {8,4,255,255,255},
    {9,4,0,166,255},
    {0,5,0,166,255},
    {1,5,255,255,255},
    {2,5,255,255,255},
    {3,5,255,255,255},
    {4,5,255,255,255},
    {5,5,255,255,255},
    {6,5,255,255,255},
    {7,5,255,255,255},
    {8,5,255,255,255},
    {9,5,0,166,255},
    {1,6,0,166,255},
    {2,6,0,255,35},
    {3,6,0,255,35},
    {4,6,0,255,35},
    {5,6,0,255,35},
    {6,6,0,255,35},
    {7,6,0,255,35},
    {8,6,0,166,255},
    {1,7,0,166,255},
    {2,7,255,255,255},
    {3,7,255,255,255},
    {4,7,255,255,255},
    {5,7,255,255,255},
    {6,7,255,255,255},
    {7,7,255,255,255},
    {8,7,0,166,255},
    {2,8,0,166,255},
    {3,8,255,255,255},
    {4,8,255,255,255},
    {5,8,255,255,255},
    {6,8,255,255,255},
    {7,8,0,166,255},
    {3,9,0,166,255},
    {4,9,0,166,255},
    {5,9,0,166,255},
    {6,9,0,166,255},
    {PICTURE_NULL,84,10,10,10},

    {0,0,0,166,255},
    {1,0,255,255,255},
    {2,0,255,255,255},
    {3,0,255,255,255},
    {4,0,255,255,255},
    {5,0,255,255,255},
    {6,0,255,255,255},
    {7,0,255,255,255},
    {8,0,255,255,255},
    {9,0,0,166,255},
    {0,1,0,166,255},
    {1,1,255,255,255},
    {2,1,255,255,255},
    {3,1,255,255,255},
    {4,1,255,255,255},
    {5,1,255,255,255},
    {6,1,255,255,255},
    {7,1,255,255,255},
    {8,1,255,255,255},
    {9,1,0,166,255},
    {0,2,0,166,255},
    {1,2,255,255,0},
    {2,2,255,255,0},
    {3,2,255,255,0},
    {4,2,255,255,0},
    {5,2,255,255,0},
    {6,2,255,255,0},
    {7,2,255,255,0},
    {8,2,255,255,0},
    {9,2,0,166,255},
    {0,3,0,166,255},
    {1,3,255,255,255},
    {2,3,255,255,255},
    {3,3,255,255,255},
    {4,3,255,255,255},
    {5,3,255,255,255},
    {6,3,255,255,255},
    {7,3,255,255,255},
    {8,3,255,255,255},
    {9,3,0,166,255},
    {0,4,0,166,255},
    {1,4,255,255,255},
    {2,4,255,255,255},
    {3,4,255,255,255},
    {4,4,255,255,255},
    {5,4,255,255,255},
    {6,4,255,255,255},
    {7,4,255,255,255},
    {8,4,255,255,255},
    {9,4,0,166,255},
    {1,5,0,166,255},
    {2,5,0,255,35},
    {3,5,0,255,35},
    {4,5,0,255,35},
    {5,5,0,255,35},
    {6,5,0,255,35},
    {7,5,0,255,35},
    {8,5,0,166,255},
    {1,6,0,166,255},
    {2,6,255,255,255},
    {3,6,255,255,255},
    {4,6,255,255,255},
    {5,6,255,255,255},
    {6,6,255,255,255},
    {7,6,255,255,255},
    {8,6,0,166,255},
    {2,7,0,166,255},
    {3,7,255,255,255},
    {4,7,255,255,255},
    {5,7,255,255,255},
    {6,7,255,255,255},
    {7,7,0,166,255},
    {3,8,0,166,255},
    {4,8,0,166,255},
    {5,8,0,166,255},
    {6,8,0,166,255},
    {PICTURE_NULL,76,10,10,10},

    {ANIMATION_NULL,6,0,0,0}

};

#endif
