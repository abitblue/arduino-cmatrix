#ifndef RANDOM_NOISE_HPP
#define RANDOM_NOISE_HPP

#include "Matrix.hpp"

#define MAX_DIMENSION ((kMatrixWidth>kMatrixHeight) ? kMatrixWidth : kMatrixHeight)

static uint16_t randX = random16();
static uint16_t randY = random16();
static uint16_t randZ = random16();

uint16_t speed = 5;         //Default 20
uint16_t scale = 20;       //Default 311

uint8_t noise[MAX_DIMENSION][MAX_DIMENSION];

void fillnoise8() {
    for(int i = 0; i < MAX_DIMENSION; i++) {
        int ioffset = scale * i;
            for(int j = 0; j < MAX_DIMENSION; j++) {
                int joffset = scale * j;
                noise[i][j] = inoise8(randX + ioffset,randY + joffset,randZ);
        }
    }
    randZ += speed;
}

void noiseLoop(uint16_t iterations, uint16_t wantScale, uint16_t wantSpeed) {
    scale = wantScale;
    speed = wantSpeed;
    for (int times=0; times<=iterations; times++)
    {
        static uint8_t ihue=0;
        fillnoise8();
        for(int i = 0; i < kMatrixWidth; i++) {
            for(int j = 0; j < kMatrixHeight; j++) {
                //leds[XYsafe(i,j)] = CHSV(noise[j][i],255,noise[i][j]);
                leds[XYsafe(i,j)] = CHSV(ihue + (noise[j][i]>>2),255,noise[i][j]);
            }
        }
        ihue+=1;
        FastLED.show();
    }
}

#endif
